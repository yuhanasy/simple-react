import React from 'react';
import ReactDOM from 'react-dom';
import './ghibli-api/ghibli.css';
import App from './ghibli-api/Ghibli';

// untuk manggil App dari Ghibli-api
// import App from './ghibli-api/Ghibli';

// untuk import App dari wikipedia-api
// import App from './wikipedia-api/Api';

ReactDOM.render(<App />, document.getElementById('root'));