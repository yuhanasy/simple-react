import React, { Component } from 'react';
import gambar from './logo.png'

class Logo extends Component {
  render() {
    return (
      <img src={ gambar } />
    )
  }
}

export default Logo;