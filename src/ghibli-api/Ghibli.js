import React, { Component } from 'react';
import Logo from './Logo';

class App extends Component {
  state = {
    data: []
  }

  componentDidMount() {
    const url = 'https://ghibliapi.herokuapp.com/films';

    fetch(url)
      .then(result => result.json())
      .then(result => {
        this.setState({
          data: result
        })
      })
  }

  handleClick = (e) => {
    console.log(this.state.data[0].title);
  }


  render() {
    let Card = () => {
      return (
        this.state.data.map((movie, index) =>{
          return (
            <div className='card' key={ index } alt='Ghibli'>
              <h1>{ movie.title} </h1>
              <p>{ movie.description.substring(0, 300) }</p>
            </div>
          )
        })
      )
    }

    return(
      <div>
        <Logo />
        <div className='container'>
          <Card />
        </div>
      </div>
      
    )
  }
}

export default App;